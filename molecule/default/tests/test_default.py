import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('postgres')


def test_psql(host):
    cmd = host.run('sudo docker exec postgres psql -U test test -c "SELECT 1;" -o /dev/null')
    assert cmd.rc == 0
