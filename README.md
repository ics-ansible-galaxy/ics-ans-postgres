ics-ans-postgres
===================

Ansible playbook to install postgres.

Requirements
------------

- ansible >= 2.4
- molecule >= 2.6

License
-------

BSD 2-clause
